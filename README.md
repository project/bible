# Drupal Bible

This Bible module provides the ability to read the Bible natively within a Drupal website. Administrators can import their own Bible Context (.bc) files into the database as entities. The module has multi-version and language support.

Module functions from the old Drupal 7 version which are planned to be ported to Drupal 10:

1. Read the Bible online. You can read the Bible within your website without any third-party tools.
2. Search Bible verses. Enter keyword and search all verses contain this keyword. Combined keyword search not supported.
3. BLS Filter. Turn on BLS Filter, and one can use [GEN:1:1] to embbed verse in node-page. Multi-verse is avaible.
4. Block Daily Golden Verse.
5. Multi-Bible reading.
6. Strong Number AJAX functions.
7. Strong Number Search in Bible.
8. Bible data of commentary, music, daily reading,etc.
9. Web Import function.
10. User-mark verse set function and "My Verse" link.
11. Inline passage. User can turn on filter function, and use BLS syntax to add passage in article.
12. Bible change selector.
13. Browser language detect choose bible.
14. Reference Verse function.
15. Bible note function.
16. Bible Gallery data.
