(function ($, Drupal) {
  Drupal.behaviors.bibleDropdown = {
    attach: function (context, settings) {
      $('#book-select').on('change', function() {
        window.location.href = '/bible/' + this.value + '/1';
      });
      // Add similar logic for version change.
    }
  };
})(jQuery, Drupal);
