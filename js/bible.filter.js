(function ($, Drupal) {
  Drupal.behaviors.bibleFilterPopup = {
    attach: function (context, settings) {
      $('.bible-reference', context).hover(
        function() {
          $(this).find('.popup').show();
        },
        function() {
          $(this).find('.popup').hide();
        }
      );
    }
  };
})(jQuery, Drupal);
