<?php

namespace Drupal\bible\Plugin\Filter;

use Drupal\bible\Entity\BibleBook;
use Drupal\bible\Entity\BibleVerse;
use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\Component\Utility\Html;

/**
 * Provides a 'Bible Reference Filter' filter.
 *
 * @Filter(
 *   id = "bible_filter",
 *   title = @Translation("Bible references as popups"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class BibleFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    // Query Bible books where the 'bible' property is 1.
    // @todo Get a configurable limitation.
    $bookIds = \Drupal::entityTypeManager()->getStorage('bible_book')->getQuery()
      ->condition('bible', 1)
      ->accessCheck(FALSE)
      ->execute();

    // Load all Bible books from the BibleBook entity.
    $books = BibleBook::loadMultiple($bookIds);

    $bookList = [];
    $bookNames = [];

    // Populate the book names array with different variations: code, name, and shortname.
    foreach ($books as $book) {
      $name = preg_quote($book->get('name')->value, '/');
      $shortname = preg_quote($book->get('shortname')->value, '/');
      $code = preg_quote($book->get('code')->value, '/');

      $bookList[$name] = $book;
      $bookList[$shortname] = $book;
      $bookList[$code] = $book;
    }

    // Remove any duplicate values from the array.
    $bookNames = array_keys($bookList);

    // Create a regex pattern to match Bible references including verse ranges.
    $book_pattern = implode('|', $bookNames);
    $pattern = '/\b(' . $book_pattern . ')[\s:]*\d+:\d+(-\d+)?\b/i';

    // Use a callback to replace the matched references with popup content and a link.
    $callback = function ($matches) use ($bookList) {
      // Extract the reference.
      $reference = $matches[0];
      $book_name = $matches[1];

      // Extract the book code from the matched book name.
      $book = $bookList[$book_name];

      // Extract the chapter and verse range from the reference.
      preg_match('/(\d+):(\d+)(-(\d+))?/', $reference, $chapter_verses);
      $chapter = $chapter_verses[1];
      $verse_start = $chapter_verses[2];
      $verse_end = !empty($chapter_verses[4]) ? $chapter_verses[4] : $verse_start;

      // Generate the link URL using the `bible.read` route and passing the book code and chapter.
      $url = Url::fromRoute('bible.read', [
        'book' => $book->get('code')->value,
        'chapter' => $chapter,
      ])->toString();

      // Query BibleVerse entities for the specified book, chapter, and verse range.
      $verse_ids = \Drupal::entityTypeManager()->getStorage('bible_verse')->getQuery()
        ->condition('book', $book->id())
        ->condition('chapter', $chapter)
        ->condition('verse', [$verse_start, $verse_end], 'BETWEEN')
        ->accessCheck(FALSE)
        ->execute();

      // Load the verses and concatenate their text.
      $verses = BibleVerse::loadMultiple($verse_ids);
      $popup_content = '';
      // @todo Render the entities instead
      foreach ($verses as $verse) {
        $popup_content .= '<span>' . Html::escape($verse->get('text')->value) . '</span>';
      }

      // Generate the popup content and wrap the reference in a link.
      return '<a href="' . $url . '" class="bible-reference" data-reference="' . $reference . '">' . $reference . '<span class="popup">' . $popup_content . '</span></a>';
    };

    // Perform the replacement using the regex pattern.
    $new_text = preg_replace_callback($pattern, $callback, $text);

    // Create a FilterProcessResult object and attach the library.
    $result = new FilterProcessResult($new_text);
    $result->addAttachments([
      'library' => [
        'bible/bible.filter',
      ],
    ]);

    return $result;
  }

}
