<?php

namespace Drupal\bible\Form;

use Drupal\bible\Entity\Bible;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Configuration form for the Bible entity type.
 */
final class BibleSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bible.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'bible_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('bible.settings');

    // We could also add an isDefault() check to the Bible entity
    // but the setting storage should be global.
    // The Bible class could also have a helper function built in.
    $form['default_bible'] = [
      '#type' => 'select',
      '#title' => $this->t('Default Bible Translation'),
      '#default_value' => $config->get('default_bible'),
      '#description' => $this->t('Select the default Bible translation to be used.'),
      '#required' => TRUE,
      '#options' => $this->getBibleOptions(),
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->configFactory->getEditable('bible.settings')
      ->set('default_bible', $form_state->getValue('default_bible'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Helper function to load Bible entities and create an options array.
   */
  protected function getBibleOptions() {
    $options = [];

    // Load all bible entities
    $bibles = Bible::loadMultiple();

    // Populate options array with bible IDs and titles
    foreach ($bibles as $bible) {
      $options[$bible->id()] = $bible->label();
    }

    if (empty($options)) {
      $url = Url::fromRoute('bible.import');
      $link = Link::fromTextAndUrl($this->t('import a Bible translation'), $url)->toString();
      $this->messenger()->addWarning($this->t('No Bible translations found. Please @link.', ['@link' => $link]));

      $options[] = '- None -';
    }

    return $options;
  }

}
