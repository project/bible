<?php

namespace Drupal\bible\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\file\FileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BibleImportForm.
 */
class BibleImportForm extends FormBase {

  protected $httpClientFactory;

  /**
   * Bible ID for file being processed.
   */
  protected $bid = -1;
  protected $bible;

  /**
   * Current Book ID for file being processed.
   */
  protected $bkid = 1;
  protected $books = [];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bible_import';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client_factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ClientFactory $httpClientFactory) {
    $this->httpClientFactory = $httpClientFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['heading'] = [
      '#markup' => '<h2>' . $this->t('Upload') . '</h2>',
    ];
    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Upload a .bc or .bc.txt file containing Bible content or select a file to download and install from the list below. You can also upload a Strong Number list with the .sn extension (note: not yet implemented).'),
    ];

    $form['upload'] = [
      '#type' => 'file',
      '#title' => $this->t('File upload'),
      '#description' => $this->t('Upload a file with the extension .bc or .bc.txt or .sn'),
    ];

    // Submit button for file upload
    $form['submit_upload'] = [
      '#type' => 'submit',
      '#value' => $this->t('Upload'),
      '#button_type' => 'primary',
    ];

    // Add a markup for the heading "Download"
    $form['download_heading'] = [
      '#markup' => '<h2>' . $this->t('Download') . '</h2>',
    ];

    // Add a descriptive item for explaining the download process
    $form['download_description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Below is a list of files that can be downloaded from GitHub. Clicking "Install" will start the download and installation process for the selected file.'),
    ];

    // Table to download a file from Github.
    $config = $this->config('bible.settings');
    $repo = $config->get('importer.github_repository');
    $client = $this->httpClientFactory->fromOptions([
      'base_uri' => 'https://api.github.com',
    ]);
    $response = $client->get("/repos/{$repo}/contents");
    $files = json_decode($response->getBody());
    $bcFiles = array_filter($files, function ($file) {
      return $file->type === 'file' && substr($file->name, -7) === '.bc.txt';
    });

    $form['files_table'] = [
      '#type' => 'table',
      '#header' => [$this->t('Name'), $this->t('File'), $this->t('Action')],
      '#empty' => $this->t('No files found. Check that the Github repository in configuration is set correctly.'),
    ];

    foreach ($bcFiles as $index => $file) {
      // Loading these names is too expensive an operation for here.
      // @todo Delay this with AJAX and cache it for a long time.
      //$name = $this->fetchAndParseFirstLine($file->download_url);
      $name = "Loading... ({$file->name})";
      $form['files_table'][$index]['name'] = [
        '#markup' => $name,
      ];
      $form['files_table'][$index]['path'] = [
        '#markup' => $file->path,
      ];
      $form['files_table'][$index]['install'] = [
        '#type' => 'submit',
        '#value' => $this->t('Install'),
        '#name' => 'install_' . $index,
        '#submit' => ['::submitDownload'],
        '#attributes' => [
          'download_url' => $file->download_url,
        ],
      ];
    }

    return $form;
  }

  /**
   * @todo
   */
  public function submitDownload(array &$form, FormStateInterface $form_state) {
    // Find out which button was clicked
    $triggering_element = $form_state->getTriggeringElement();
    $uri = $triggering_element['#attributes']['download_url'];

    $this->messenger()->addMessage($this->t('Installing file: @path', ['@path' => $uri]));

    $this->parseBibleContext($uri);
  }

  /**
   * @todo Move this to a BC parsed helper class
   *
   * This is cool but still too slow for 36 files. We could AJAX it
   * and/or cache the results.
   */
  function fetchAndParseFirstLine($download_url) {
    $context = stream_context_create([
      "http" => [
        "method" => "GET",
        "header" => "User-Agent: DrupalBible/1.0" // GitHub requires a user agent
      ]
    ]);

    $handle = @fopen($download_url, "r", false, $context);

    if ($handle) {
      $lineCount = 0;
      while (($line = fgets($handle)) !== false && $lineCount < 20) {
        // Increment line count so we stop looking after a reasonable number
        $lineCount++;

        // Strip out Unicode characters from the start of the line
        $line = preg_replace('/^[\p{Z}\p{C}]+/u', '', $line);

        // Trim the line to remove any whitespace
        $trimmedLine = trim($line);

        // Skip empty lines or lines starting with ^, *, or #
        if (empty($trimmedLine) || preg_match('/^[*^#]/', $trimmedLine)) {
          continue;
        }

        // This line is the first non-empty, not starting with ^, *, or #
        fclose($handle); // Close the stream
        return $trimmedLine; // Return the line
      }
      fclose($handle); // Make sure to close the stream if we didn't return early
    } else {
      // Handle error opening the URL
      return null;
    }

    // No suitable line found or couldn't open file
    return null;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate file upload
    $all_files = $this->getRequest()->files->get('files', []);
    if (!empty($all_files['upload'])) {
      $file_upload = $all_files['upload'];
      if (!in_array($file_upload->getClientOriginalExtension(), ['bc', 'txt', 'sn'])) {
        $form_state->setErrorByName('upload', $this->t('Only files with the following extensions are allowed: .bc, .txt, .sn.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Handle file upload
    if (!empty($_FILES['files']['name']['upload'])) {
      $file = file_save_upload('upload', [
        'file_validate_extensions' => ['bc txt sn'], // Validate extensions
      ], FALSE, 0, FileSystemInterface::EXISTS_REPLACE);

      if ($file) {
        // Store the file so it can be re-used.
        $file->setPermanent();
        $file->save();
        // Parse the file (@todo hand this off to a parser service)
        $uri = $file->getFileUri();
        $this->parseBibleContext($uri);
      }
      else {
        $this->messenger()->addError($this->t('File upload failed.'));
      }
    }
  }

  /**
   * Converts encoding from charset X to utf8 if needed.
   */
  protected function convertCharacterEncoding(string $text) {
    $characterEncoding = mb_detect_encoding($text, 'UTF-8, UTF-16, ISO-8859-1, ISO-8859-15, Windows-1252, ASCII');
    switch ($characterEncoding) {
      case 'UTF-8':
        // Do nothing.
        break;

      case 'ISO-8859-1':
        $text = utf8_encode($text);
        break;

      default:
        $text = mb_convert_encoding($text, 'UTF-8', $characterEncoding);
        break;
    }
    return $text;
  }

  /**
   * @todo
   */
  protected function getExistingBibleId($shortName, $language) {
    return NULL;
  }

  /**
   * This is old code migrated and should be reviewed for refactoring.
   *
   * @todo This needs to be implemented as a BibleParser plugin.
   */
  protected function parseBibleContext(string $uri) {
    $fd = fopen($uri, 'rb');
    if (empty($fd)) {
      $this->messenger()->addError($this->t('The Bible import failed as the file @filename could not be read.', [
        '@filename' => $file->filename,
      ]));
      return FALSE;
    }

    // If not in 'safe mode', increase the maximum execution time.
    if (!ini_get('safe_mode')) {
      set_time_limit(240);
    }

    $status = '';
    $lineno = 0; // Current line
    while (!feof($fd)) {
      // Place reasonable limit on length of line being processed.
      $line = fgets($fd, 10 * 1024);
      $lineno++;
      $line = trim(strtr($line, array("\\\n" => '')));
      if (strlen($line) > 0) {
        $this->processLine($line, $status);
      }
    }

    $this->messenger()->addMessage($this->t('Import completed with a total line count of '. $lineno));
    // @todo Set default Bible if not one set already.
    // @todo Set watchdog with total line numbers and entities created.
    // @todo Redirect to Bible list?
  }

  /**
   * Creates Bible entities from a given line.
   */
  protected function processLine($line, &$status) {
    switch (substr($line, 0, 1)) {
      case '*': // Section Declare.
        if (strncmp('*Bible', $line, 6) == 0) {
          $status = 'B';
        }
        if (strncmp('*Chapter', $line, 8) == 0) {
          $status = 'C';
          // @todo there was a "delete book name" entity call here.
        }
        if (strncmp('*Context', $line, 8) == 0) {
          $status = 'V';
          // @todo there was a "delete bible context" entity call here.
          // to stop verses being duplicated?
        }
        break;

      case '#': // Comment.
        break;

      case '^':  // Variable Setting.
        if (strncmp('^Bible', $line, 6) == 0) {
          $line = $this->convertCharacterEncoding($line);
          $data = explode('|', $line);
          $this->bid = $this->getExistingBibleId($data[1], $data[2]);
          if ($this->bid === '') {
            $message = t('Existing Bible not found. Bible name: @bsn, language: @lang.', [
              '@bsn' => $data[1],
              '@lang' => $data[2],
            ]);
            $this->messenger()->addError($message);
            return FALSE;
          }
          if (count($data) > 3) {
            // @todo update bible entity serial number?
            // Based on my understanding here, the ^ only allows for
            // extra verses to be added and the version string to be updated.
          }
        }
        if (strncmp('^Context', $line, 8) == 0) {
          $status = 'V';
        }
        break;

      default: // Context Data.
        $line = $this->convertCharacterEncoding($line);
        $data = explode('|', $line);

        switch ($status) {
          case 'B':
            /** @var \Drupal\bible\Entity\Bible $bible */
            $bible = \Drupal::entityTypeManager()
              ->getStorage('bible')
              ->create([
                'shortname' => $data[0],
                'name' => $data[1],
                'langcode' => $data[2], // @todo This is not mapped properly
                'version' => $data[3] ?? NULL,
              ]);

            // Save the entity.
            $this->bid = $bible->save();
            $this->bible = $bible;
            break;

          case 'C':
            if ($this->bid > -1) {
              $book = \Drupal::entityTypeManager()
                ->getStorage('bible_book')
                ->create([
                  'bible' => $this->bible,
                  'number' => $this->bkid++,
                  'code' => $data[0],
                  'name' => $data[1],
                  'shortname' => $data[2],
                  'chapters' => $data[3],
                ]);
              $book->save();
              $this->books[$data[0]] = $book;
            }
            break;

          case 'V':
            if ($this->bid > -1) {
              $verse = \Drupal::entityTypeManager()
                ->getStorage('bible_verse')
                ->create([
                  'bible' => $this->bible,
                  'book' => $this->books[$data[0]], // @todo helper function
                  'chapter' => $data[1],
                  'verse' => $data[2],
                  'linemark' => $data[3],
                  'text' => $data[4],
                ]);
              $verse->save();
            }
            break;

        }
        break;
    }
  }

}
