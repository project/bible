<?php

namespace Drupal\bible;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for the bible entity type.
 */
final class BibleListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['label'] = $this->t('Name');
    $header['shortname'] = $this->t('Abbreviation');
    $header['langcode'] = $this->t('Language');
    $header['version'] = $this->t('Source Version');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Updated');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\bible\BibleInterface $entity */
    $row['id'] = $entity->id();
    $row['label'] = $entity->toLink();
    $row['shortname']['data'] = $entity->get('shortname')->view(['label' => 'hidden']);
    $row['langcode']['data'] = $entity->get('langcode')->view(['label' => 'hidden']);
    $row['version']['data'] = $entity->get('version')->view(['label' => 'hidden']);
    $row['created']['data'] = $entity->get('created')->view(['label' => 'hidden']);
    $row['changed']['data'] = $entity->get('changed')->view(['label' => 'hidden']);
    return $row + parent::buildRow($entity);
  }

}
