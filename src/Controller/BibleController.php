<?php

namespace Drupal\bible\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class BibleController extends ControllerBase {

  protected $previousBook;
  protected $currentBook;
  protected $nextBook;

  protected $currentChapter;

  protected $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  public function read(Request $request, $book = 'GEN', $chapter = 1) {
    // @todo Load default version and confirm it exists
    // else we want to say "No Bible has been imported. Contact your administrator."
    $defaultVersion = 1;
    $this->currentChapter = $chapter;

    $build = [
      '#theme' => 'bible_read',
      '#books' => $this->getBooks($defaultVersion, $book),
      '#versions' => $this->getVersions(),
      '#current_book' => $book,
      '#current_chapter' => $chapter,
      '#verses' => $this->getVerses(),
      '#prev_book' => $this->previousBook?->get('code')->value,
      '#prev_chapter' => --$chapter ?? 1, // @todo check if valid
      '#next_chapter' => ++$chapter, // @todo check if valid
      '#next_book' => $this->nextBook?->get('code')->value,
      '#attached' => [
        'library' => ['bible/bible.read'],
      ],
    ];

    return $build;
  }

  /**
   * Fetches all books for a given Bible version.
   *
   * @param int $bibleId
   *   The ID of the Bible version to fetch books for. Defaults to 1.
   *
   * @return array
   *   An array of BibleBook entities where the key is the book's code and the value is its name.
   */
  protected function getBooks($bibleId = 1, $currentBookCode = 'GEN') {
    $bookStorage = $this->entityTypeManager->getStorage('bible_book');
    $bookIds = $bookStorage->getQuery()
      ->condition('bible', $bibleId)
      ->sort('id', 'ASC')
      ->accessCheck(FALSE)
      ->execute();
    $books = $bookStorage->loadMultiple($bookIds);
    $bookList = [];
    foreach ($books as $book) {
      $bookList[$book->get('code')->value] = $book->get('name')->value;
      // Logic for which book might be previous/current/next in the list.
      if (!$this->nextBook && $this->currentBook) {
        $this->nextBook = $book;
      }
      if ($book->get('code')->value === $currentBookCode) {
        $this->currentBook = $book;
      }
      if (!$this->currentBook) {
        $this->previousBook = $book;
      }
    }

    return $bookList;
  }

  protected function getVersions() {
    $bibleStorage = $this->entityTypeManager->getStorage('bible');
    $bibleIds = $bibleStorage->getQuery()
      ->sort('id', 'ASC')->accessCheck(FALSE)->execute();
    $bibles = $bibleStorage->loadMultiple($bibleIds);
    $bibleList = [];
    foreach ($bibles as $bible) {
      $bibleList[$bible->get('shortname')->value] = $bible->get('name')->value;
    }

    return $bibleList;
  }

  protected function getVerses() {
    $verseStorage = $this->entityTypeManager->getStorage('bible_verse');
    $verseIds = $verseStorage->getQuery()
      ->condition('book', $this->currentBook?->id())
      ->condition('chapter', $this->currentChapter)
      ->sort('id', 'ASC')
      ->accessCheck(FALSE)
      ->execute();
    $verses = $verseStorage->loadMultiple($verseIds);
    $verseList = [];
    foreach ($verses as $verse) {
      $verseList[] = [
        'chapter' => $verse->get('chapter')->value,
        'verse' => $verse->get('verse')->value,
        'text' => $verse->get('text')->value,
      ];
    }

    return $verseList;
  }

}
