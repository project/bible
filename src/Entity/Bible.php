<?php declare(strict_types = 1);

namespace Drupal\bible\Entity;

use Drupal\bible\BibleInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Bible entity class.
 *
 * @ContentEntityType(
 *   id = "bible",
 *   label = @Translation("Bible"),
 *   label_collection = @Translation("Bibles"),
 *   label_singular = @Translation("bible"),
 *   label_plural = @Translation("bibles"),
 *   label_count = @PluralTranslation(
 *     singular = "@count bibles",
 *     plural = "@count bibles",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\bible\BibleListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\bible\Form\BibleForm",
 *       "edit" = "Drupal\bible\Form\BibleForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "bible",
 *   admin_permission = "administer bible",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "collection" = "/admin/content/bible",
 *     "add-form" = "/bible/add",
 *     "canonical" = "/bible/{bible}",
 *     "edit-form" = "/bible/{bible}/edit",
 *     "delete-form" = "/bible/{bible}/delete",
 *     "delete-multiple-form" = "/admin/content/bible/delete-multiple",
 *   },
 *   field_ui_base_route = "entity.bible.settings",
 * )
 */
class Bible extends ContentEntityBase implements BibleInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entityType): array {

    $fields = parent::baseFieldDefinitions($entityType);

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of this Bible entity.'))
      ->setReadOnly(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The full name of this Bible translation.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['shortname'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Short Name'))
      ->setDescription(t('The abbreviation of this Bible translation.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language'))
      ->setDescription(t('The language code of the Bible translation.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['version'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Source Version'))
      ->setDescription(t('The version string of the import file.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ]);
;

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Imported on'))
      ->setDescription(t('The time that the Bible entity was imported.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the Bible was last edited.'));

    return $fields;
  }

  /**
   * Gets all bible_book entities that reference this Bible.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of bible_book entity objects.
   */
  public function getBooks() {
    $storage = \Drupal::entityTypeManager()->getStorage('bible_book');

    $ids = $storage->getQuery()
      ->condition('bible', $this->id())
      ->accessCheck(TRUE)
      ->execute();

    return $storage->loadMultiple($ids);
  }

}
