<?php

namespace Drupal\bible\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;

/**
 * Defines the Bible Verse entity.
 *
 * @ContentEntityType(
 *   id = "bible_verse",
 *   label = @Translation("Bible Verse"),
 *   base_table = "bible_verse",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "text",
 *     "uuid" = "uuid",
 *   },
 *   admin_permission = "administer bible",
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   links = {
 *     "collection" = "/admin/content/bible",
 *     "canonical" = "/bible/verse/{bible_verse}",
 *   },
 *   field_ui_base_route = "entity.bible_verse.settings",
 * )
 */
class BibleVerse extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entityType) {
    $fields = parent::baseFieldDefinitions($entityType);

    $fields['bible'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Bible Reference'))
      ->setDescription(t('The reference to the Bible entity.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'bible')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['book'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Book Reference'))
      ->setDescription(t('The reference to the Bible Book entity.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'bible_book')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['chapter'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Chapter'))
      ->setDescription(t('The chapter number of the verse.'))
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['verse'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Verse number'))
      ->setDescription(t('The verse number of the verse.'))
      ->setSetting('unsigned', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['linemark'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Linemark'))
      ->setDescription(t('The linemark of the verse.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['text'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Text'))
      ->setDescription(t('The text of the verse.'))
      ->setSetting('text_processing', 0)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['superscription'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Superscription'))
      ->setDescription(t('The superscription is used to add a prefix before the first verse of a chapter or Psalm.'))
      ->setSetting('text_processing', 0);

    $fields['subscription'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Subscription'))
      ->setDescription(t('The subscription is sometimes used to suffix a comment to the final verse of a book.'))
      ->setSetting('text_processing', 0);

    return $fields;
  }

}
