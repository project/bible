<?php

namespace Drupal\bible\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the Bible Concordance entity.
 *
 * @ContentEntityType(
 *   id = "bible_concordance",
 *   label = @Translation("Bible Concordance"),
 *   base_table = "bible_concordance",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *   },
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *       "add" = "Drupal\Core\Entity\ContentEntityForm",
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityForm",
 *     },
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   links = {
 *     "canonical" = "/bible/concordance/{bible_concordance}",
 *     "add-form" = "/bible/concordance/add",
 *     "edit-form" = "/bible/concordance/{bible_concordance}/edit",
 *     "delete-form" = "/bible/concordance/{bible_concordance}/delete",
 *     "collection" = "/bible/concordance/list"
 *   },
 * )
 */
class BibleConcordance extends ContentEntityBase {
  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name and number of the concorance entry.'))
      ->setSettings(['max_length' => 255])
      ->setRequired(TRUE);

    $fields['content'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Content'))
      ->setDescription(t('The content of the concordance entry.'));

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code of the concordance.'));

    $fields['concordance'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Concordance'))
      ->setDescription(t('The title of the concordance.'))
      ->setSettings(['max_length' => 255])
      ->setRequired(TRUE);

    return $fields;
  }
}
