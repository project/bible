<?php declare(strict_types = 1);

namespace Drupal\bible;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a bible entity type.
 */
interface BibleInterface extends ContentEntityInterface, EntityChangedInterface {

}
